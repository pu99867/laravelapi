<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

class CategoryController extends Controller
{
    
    public function index()
    {
        $response = Http::get('http://103.175.221.5:8001/category')->json()['result']['data'];

        return view('category.index', compact('response'));
    }




    public function store(Request $request)
{
    $request->validate([
        'name' => 'required|string|max:255',
    ]);

    $response = Http::post('http://103.175.221.5:8001/category', [
        'name' => $request->name
    ]);

    if ($response->successful()) {
        // Jika permintaan berhasil, simpan pesan sukses di session
        Session::flash('api_response', 'Data has been successfully saved.');
    } else {
        // Jika permintaan gagal, simpan pesan gagal di session 
        Session::flash('api_response', 'Failed to save data. Please try again.');
    }

    return redirect()->route('categorys.create');

}





    public function edit($id)
    {
    
        $response = Http::get('http://103.175.221.5:8001/category'.$id)->json();
        return view('category.edit', compact('response'));

    }



}

