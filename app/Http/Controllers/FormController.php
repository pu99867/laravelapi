<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

class FormController extends Controller
{
    public function index()
    {
        $response = Http::get('http://103.175.221.5:8001/form')->json()['result']['data'];

        return view('form.index', compact('response'));
    }
}
