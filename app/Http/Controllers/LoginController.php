<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index()
    {
        return view('log.index');
    }

    public function authenticate(Request $request)
{
    $username = $request->input('username');
    $password = $request->input('password');

    $response = Http::post('http://localhost:8001/login', [
        'username' => $username,
        'password' => $password,
    ]);

    if ($response->successful()) {
        $responseData = $response->json();
        $apiToken = $responseData['result']['token'];

        Session::put('token', $apiToken);

        return redirect()->route('dashboard');
    } else {
        $errorMessage = 'Login failed. Please try again with correct credentials.';
        return view('log.index')->with('error', $errorMessage);
    }
}

}









    


