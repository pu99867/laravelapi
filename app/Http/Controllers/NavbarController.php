<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class NavbarController extends Controller
{
    public function index()
    {
        $response = Http::get('http://localhost:8001/navbar')->json()['result']['data'];

        return view('navbar.index', compact('response'));
    }
}
