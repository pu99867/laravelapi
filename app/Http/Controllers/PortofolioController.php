<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Http;
use App\Models\portofolio;
use Session;
// Sesuaikan dengan model Anda

class PortofolioController extends Controller
{
    public function index()
    {
        $response = Http::get('http://localhost:8001/portfolio')->json()['result']['data'];

        return view('portofolio.index', compact('response'));
    }

    public function portofolio()
    {
        return view('portofolio.create');
    }

    public function portofolio_proses(Request $request)
    {
        $token = Session::get('token');

        if (!$token) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        // Validasi input
        $request->validate([
            'Image' => 'required|image|mimes:jpeg,jpg,png|max:2048',
            'Title' => 'required',
            'Id_category' => 'required|integer',
        ]);

        // Mengambil file gambar yang diunggah dari form-data
        $image = $request->file('Image');
        $title = $request->input('Title');
        $id_category = (int)$request->input('Id_category');

        // Dapatkan nama file gambar yang diunggah
        $imageName = $image->getClientOriginalName();

        // Mengirimkan permintaan POST dengan data form-data ke server lain
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->attach('Image', file_get_contents($image), $imageName) // Menggunakan 'attach' untuk mengirim file
            ->post('http://localhost:8001/portfolio/', [
                'Title' => $title,
                'Id_category' => $id_category,
            ]);

        if ($response->successful()) {
            return redirect()->route('portofolio.index')->with(['success' => 'Data Berhasil Disimpan!']);
        } else {
            return redirect()->route('portofolio.portofolio')->with(['error' => 'Gagal menyimpan data']);
        }
    }

    public function portofolio_edit(string $id)
    {
        $portfolio = portofolio_edit::findOrFail($id);

        return view('portofolio.edit', compact('portfolio'));
    }

    public function portofolio_update(Request $request, $id)
    {
        $token = Session::get('token');

        // Validasi input
        $request->validate([
            'image' => 'image|mimes:jpeg,jpg,png|max:2048',
            'title' => 'required',
            'id_category' => 'required',
        ]);

        // Mengambil data yang ingin diupdate
        $portfolio = Portofolio::findOrFail($id);
        
        // Membuat array 
        $dataToUpdate = [
            'title' => $request->input('title'),
            'id_category' => (int)$request->input('id_category'),
        ];

        // Jika ada file gambar yang diunggah, tambahkan ke data yang akan diupdate
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imagePath = $image->store('public/posts');
            $dataToUpdate['image'] = $imagePath;
        }

        // Mengirimkan permintaan PUT atau PATCH ke server API
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->patch("http://localhost:8001/portfolio/", $dataToUpdate);

        if ($response->successful()) {
            return redirect()->route('portofolio.index')->with('success', 'Data berhasil diupdate');
        } else {
            return redirect()->route('portofolio.edit', $id)->with('error', 'Gagal mengupdate data');
        }
    }
}
