<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Session;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function index()
    {
        $response = Http::get('http://localhost:8001/service')->json()['result']['data'];

        return view('service.index', compact('response'));
    }

    public function Service()
    {
        return view('service.create');
    }

    public function service_proses(Request $request)
    {
        $token = Session::get('token');
    
        if (!$token) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    
        $icon = $request->input('icon');
        $title = $request->input('title');
        $description = $request->input('description');
        
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $token, 
        ])->post('http://localhost:8001/service', [
            'icon' => $icon,
            'title' => $title,
            'description' => $description
        ]);
    
        return $response;
    }
    

    public function edit_service()
    {
        return view('service.edit', compact('response'));
    }

    public function update()
    {
    }
}
