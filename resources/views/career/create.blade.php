@extends('layouts.app')
@section('title', 'career')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Career
                    <a href="{{route ('career')}}"></a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('career-proses') }}">
                        @csrf
                        
                        <div class="form-group row">
                            <label for="id_category" class="col-md-4 col-form-label text-md-right">Category ID</label>

                            <div class="col-md-6">
                                <input id="id_category" type="text" class="form-control @error('id_category') is-invalid @enderror" name="id_category" value="{{ old('id_category') }}" required autofocus>

                                @error('id_category')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="id_position" class="col-md-4 col-form-label text-md-right">Position ID</label>

                            <div class="col-md-6">
                                <input id="id_position" type="text" class="form-control @error('id_position') is-invalid @enderror" name="id_position" value="{{ old('id_position') }}" required>

                                @error('id_position')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">Description</label>

                            <div class="col-md-6">
                                <input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required>

                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="required" class="col-md-4 col-form-label text-md-right">Required</label>

                            <div class="col-md-6">
                                <input id="required" type="text" class="form-control @error('required') is-invalid @enderror" name="required" value="{{ old('required') }}" required>

                                @error('required')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="flag" class="col-md-4 col-form-label text-md-right">Flag</label>

                            <div class="col-md-6">
                                <input id="flag" type="text" class="form-control @error('flag') is-invalid @enderror" name="flag" value="{{ old('flag') }}" required>

                                @error('flag')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Create Career
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
