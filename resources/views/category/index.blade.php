@extends('layout.app')
@section('title', 'category')
@section('content')
<h1 class="h3 mb-2 text-gray-800">Tables</h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">DataTables category</h6>
    </div>
    <div class="card-body"><a href="{{ route('categorys.store') }}" class="btn btn-md btn-primary btn-sm"><i
                class="fas fa-plus"></i>add</a>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

                <thead>
                    <tr>
                        <th scope="col">NAME</th>
                        <th scope="col">AKSI</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($response as $post)
                    <tr>

                    <td>{{ $post['name'] }}</td>
                        <a href="{{route('category.edit', $data['id'])}}">edit</a>
                    </tr>
                    @empty
                    <div class="alert alert-danger">
                        Data Post belum Tersedia.
                    </div>
                    @endforelse
                </tbody>
            </table>
        </div>

        </tbody>
        </thead>
        </table>
    </div>
</div>
</div>
@endsection