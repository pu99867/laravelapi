@extends('layout.app')
@section('title', 'form')
@section('content')
<h1 class="h3 mb-2 text-gray-800">Tables</h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">DataTables form</h6>
    </div>
    <div class="card-body"><i
                class="fas fa-plus"></i>add</a>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

                <thead>
                    <tr>
                        <th scope="col">NAME</th>
                        <th scope="col">EMAIL</th>
                        <th scope="col">MESSAGE</th>


                        <th scope="col">AKSI</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($response as $post)
                    <tr>

                        <td>{{ $post['name'] }}</td>
                        <td>{{ $post['email'] }}</td>
                        <td>{{ $post['message'] }}</td>



                        
                    </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>
        </div>

        </tbody>
        </thead>
        </table>
    </div>
</div>
</div>

@endsection