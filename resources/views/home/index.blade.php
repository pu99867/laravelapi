@extends('layout.app')
@section('title', 'home')
@section('content')
<h1 class="h3 mb-2 text-gray-800">Tables</h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">DataTables form</h6>
    </div>
    <div class="card-body"><i
                class="fas fa-plus"></i>add</a>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

                <thead>
                    <tr>
                        <th scope="col">background_image_section_1</th>
                        <th scope="col">title_section_1</th>
                        <th scope="col">description_section_1</th>
                        <th scope="col">button_section_1</th>
                        <th scope="col">sub_titlle_section_2</th>
                        <th scope="col">title_section_2</th>
                        <th scope="col">description_section_2</th>
                        <th scope="col">button_section_1</th>
                        <th scope="col">image_section_2</th>
                        <th scope="col">sub_titlle_section_3</th>
                        <th scope="col">title_section_3</th>
                        <th scope="col">sub_title_section_4</th>
                        <th scope="col">title_section_4</th>
                        <th scope="col">description_contact_us</th>
                        <th scope="col">button_contact_us</th>
                        <th scope="col">sub_title_section_5</th>
                        <th scope="col">title_section_5</th>
                        <th scope="col">link_facebook</th>
                        <th scope="col">link_linkedln</th>
                        <th scope="col">link_instagram</th>
                        <th scope="col">aksi</th>


                        <th scope="col">AKSI</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($response as $post)
                    <tr>

                        <td>{{ $post['background_image_section_1'] }}</td>
<td>{{ $post['title_section_1'] }}</td>
<td>{{ $post['description_section_1'] }}</td>
<td>{{ $post['button_section_1'] }}</td>
<td>{{ $post['sub_title_section_2'] }}</td>
<td>{{ $post['title_section_2'] }}</td>
<td>{{ $post['description_section_2'] }}</td>
<td>{{ $post['button_section_1'] }}</td>
<td>{{ $post['image_section_2'] }}</td>
<td>{{ $post['sub_title_section_3'] }}</td>
<td>{{ $post['title_section_3'] }}</td>
<td>{{ $post['sub_title_section_4'] }}</td>
<td>{{ $post['title_section_4'] }}</td>
<td>{{ $post['description_contact_us'] }}</td>
<td>{{ $post['button_contact_us'] }}</td>
<td>{{ $post['sub_title_section_5'] }}</td>
<td>{{ $post['title_section_5'] }}</td>
<td>{{ $post['link_facebook'] }}</td>
<td>{{ $post['link_linkedln'] }}</td>
<td>{{ $post['link_instagram'] }}</td>



                        
                    </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>
        </div>

        </tbody>
        </thead>
        </table>
    </div>
</div>
</div>

@endsection