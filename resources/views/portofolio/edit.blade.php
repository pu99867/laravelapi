@extends('layout.app')
@section('title', 'portofolio')
@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">EDIT Portofolio</div>
        
        <div class="card-body">
            <form method="POST" action="{{ route('portofolio-update') }}">
                @csrf

                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" name="Image" class="form-control-file" required>
                </div>

                <div class="form-group">
                    <label for="id_category">Category ID</label>
                    <input type="text" name="Id_category" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="Title" class="form-control" required>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
