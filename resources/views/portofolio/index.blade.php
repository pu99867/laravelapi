@extends('layout.app')
@section('title', 'portofolio')
@section('content')
<h1 class="h3 mb-2 text-gray-800">Tables</h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">DataTables Portofolio</h6>
    </div>
    <div class="card-body">
      <a href="{{route ('portofolio-create')}}" method="POST"><b>add</b></a>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                
            <thead>
                              <tr>
                                <th scope="col">IMAGE</th>
                                <th scope="col">TITLE</th>
                                <th scope="col">ID_CATEGORY</th>
                                <th scope="col">AKSI</th>


                              </tr>
                            </thead>
                            <tbody>
                              @forelse ($response as $post)
                                <tr>
                                  <td class="text-center">
                                    <img src="{{ $post['image'] }}" class="rounded" style="width: 150px">
                                </td>
                                    {{-- <td>{{ $post['image'] }}</td> --}}
                                    <td>{{ $post['title'] }}</td>
                                    <td>{{ $post['id_category'] }}</td>

                                  <td class="text-center">

                                    <a class="btn btn-sm btn-danger">DELETE</a>
                                    <a href="{{ route('portofolio-edit', ['id' => $post['id']]) }}" class="btn btn-sm btn-primary">EDIT</a>
                                    @csrf
                                  </td>
                                  
                                </tr>
                              
                            @empty
                                
                            @endforelse
                            </tbody>
                          </table>  
                    </div>

                </tbody>
</thead>
            </table>
        </div>
    </div>
</div>
@endsection