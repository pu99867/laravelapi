@extends('layout.app')
@section('title', 'service')
@section('content')
<h1 class="h3 mb-2 text-gray-800">Tables</h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">DataTables Service</h6>
    </div>
    <div class="card-body">
      <a href="{{route ('service-create')}}" method="POST"><b>add</b></a>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                
            <thead>
                              <tr>
                                <th scope="col">ICON</th>
                                <th scope="col">TITLE</th>
                                <th scope="col">DESCRIPTION</th>
                                <th scope="col">AKSI</th>


                              </tr>
                            </thead>
                            <tbody>
                              @forelse ($response as $post)
                                <tr>
                                  <img src="{{ $post['icon'] }}" class="rounded" style="width: 150px">
                                  <td class="text-center">
                                    <td>{{ $post['title'] }}</td>
                                  </td>
                                  <td>
                                    <a href="{{ route('edit-service', $post['id']) }}" class="btn btn-primary btn-sm">Edit</a>
                                    <td>{{ $post['description'] }}</td>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                  </form>
                                  <form action="{{ route('delete-service', $post['id']) }}" method="POST" style="display: inline;">
                                        </td>
                                        
                                    @empty
                                        
                              @endforelse
                                  </tr>
                            </tbody>
                          </table>  
                    </div>

                </tbody>
</thead>
            </table>
        </div>
    </div>
</div>
@endsection
