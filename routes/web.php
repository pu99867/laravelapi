<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SbController;
use App\Http\Controllers\FormController;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\CareerController;
use App\Http\Controllers\NavbarController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\PortofolioController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::resource('/navbars', \App\Http\Controllers\NavbarController::class);
Route::resource('/services', \App\Http\Controllers\ServiceController::class);
Route::resource('/forms', \App\Http\Controllers\FormController::class);
Route::resource('/portofolios', \App\Http\Controllers\PortofolioController::class);
Route::resource('/home', \App\Http\Controllers\HomeController::class);
Route::resource('/careers', \App\Http\Controllers\CareerController::class);
Route::resource('/positions', \App\Http\Controllers\PositionController::class);

//-------------------------------------------------------------------------------------------

Route::get('/categorys/{id}/edit', [\App\Http\Controllers\CategoryController::class, 'edit']);  
Route::put('/categories/{id}', 'CategoryController@update')->name('categorys.update');
Route::get('/', [\App\Http\Controllers\LoginController::class, 'index']);
Route::post('/authenticate', [LoginController::class, 'authenticate'])->name('login.authenticate');
Route::get('/login', [LoginController::class, 'store'])->name('store');
Route::get('/log.index', [LoginController::class, 'index']);

//---------------------------------------------------------------------------------------------------



//-------------------------------------------------------------------------
Route::controller(SbController::class)->prefix('sb')->group(function(){
    Route::get('', 'index')->name('sb');
});
//--------------------------------------------------------------------------
//ROUTE CAREER
Route::get('/career', [CareerController::class, 'index'])->name('career');
Route::post('/career-proses', [CareerController::class, 'career_proses'])->name('career-proses');
//ROUTE SERVICE
Route::get('/service-create', [ServiceController::class, 'service'])->name('service-create');
Route::post('/service-proses', [ServiceController::class, 'service_proses'])->name('service-proses');
//ROUTE PORTOFOLIO
Route::get('/portofolio-create', [PortofolioController::class, 'portofolio'])->name('portofolio-create');
Route::post('/portofolio-proses', [PortofolioController::class, 'portofolio_proses'])->name('portofolio-proses');
Route::get('/portofolio-edit', [PortofolioController::class, 'portofolio_edit'])->name('portofolio-edit');
Route::get('/portofolio-update{id}', [PortofolioController::class, 'portofolio_update'])->name('portofolio-update');
